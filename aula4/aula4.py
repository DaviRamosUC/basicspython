# Tipos primitivos

inteiro = 10
flutuante = 10.33
booleano = True & False

print(inteiro, flutuante, booleano)
print(type(inteiro), type(flutuante), type(booleano))
print(bool(''))

# Exercicio

print('Nome:', 'Davi Ramos', type('Nome: Davi Ramos'))
print('Idade:', 22, type(22))
print('Altura:', 1.76, type(1.76))
print('É maior de idade:', 22>18, type((22 > 18)))
