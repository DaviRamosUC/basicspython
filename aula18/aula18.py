"""
* Enumerate - Enumerar elementos da lista # list
"""

lista = [
    ['Edu', 'João', 'Luiz'],
    ['Maria', 'Aline', 'Joana'],
    ['Helena', 'Ed', 'Lu']
]

for indice, valor in enumerate(lista):
    print(indice, valor)
