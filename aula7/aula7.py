nome = 'Davi Ramos Lima'
idade = 22
altura = 1.76
e_maior = idade > 18
peso = 82.0
imc = peso / (altura ** 2)

print(nome, 'tem', idade, 'anos de idade e seu imc é', imc)
print(f'{nome} tem {idade} anos de idade e seu imc é {imc:.2f} ')

print('{0} tem anos de {1} e seu imc é {2:.2f}'.format(nome, idade, imc))
