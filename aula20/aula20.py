'''
    Operador ternário em Py
'''

logged_user = False

# Método convêncional
# if logged_user:
#     msg = 'Usuário logado'
# else:
#     msg = 'Usuário precisa logar'

msg = 'Usuário logado' if logged_user else 'Usuário precisa logar'
print(msg)
