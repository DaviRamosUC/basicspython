num_1 = 10
num_2 = 3
divisao = num_1 / num_2
print('valor antigo:', divisao)
print('Valor formatado:', '{:.2f}'.format(divisao))

num_1 = 1
print(f'{num_1:0>2}')

num_2 = 1150
print(f'{num_2:0<10}')  # adiciona 0 a esquerda para completar o range de 10 números
print(f'{num_2:0^10}')  # adiciona 0 entre o valor para completar o range de 10 números
print(f'{num_2:0>8.2f}')  # adiciona 0 entre o valor para completar o range de 10 números

nome = 'Davi Ramos'
print(nome[0])
