def validarCPF(cpf):
    cpf = trataCPF(cpf)
    corpoCpf = retornaCorpo(cpf)
    primeiroDigito = calculaPrimeiroDigito(corpoCpf)
    corpoCpf += primeiroDigito
    newCpf = corpoCpf + calculaSegundoDigito(corpoCpf)
    print('Válido' if newCpf == cpf else 'Inválido')


def calculaPrimeiroDigito(corpoCpf):
    soma = 0
    for i, c in enumerate(range(10, 1, -1)):
        soma += int(corpoCpf[i]) * c
    digito = (11 - (soma % 11))
    return str(digito) if digito <= 9 else '0'


def calculaSegundoDigito(corpoCpf):
    soma = 0
    for i, c in enumerate(range(11, 1, -1)):
        soma += int(corpoCpf[i]) * c
    digito = (11 - (soma % 11))
    return str(digito) if digito <= 9 else '0'


def retornaCorpo(cpf):
    return cpf[0:(len(cpf) - 2)]


def trataCPF(cpf):
    cpf = cpf.strip()
    cpf = cpf.replace('.', '')
    cpf = cpf.replace('-', '')
    return cpf


if __name__ == '__main__':
    cpf = input('Informe seu CPF: ')
    validarCPF(cpf)
