def exercicio01():
    numero = input("Digite um número inteiro: ")
    try:
        numero = int(numero)
        if numero % 2 == 0:
            print(f"{numero} é par")
        else:
            print(f"{numero} é ímpar")
    except:
        print(f"{numero} não é um inteiro")


def exercicio02():
    horario = input("Que horas são? ")
    try:
        horario = int(horario)
        if 0 <= horario <= 11:
            print("Bom dia")
        elif 12 <= horario <= 17:
            print('Boa tarde')
        elif 18 <= horario <= 23:
            print('Boa noite')
        else:
            print('Valor deve estar entre 0 e 23')
    except:
        print(f"{horario} não é um valor válido")


def exercicio03():
    nome = input("Informe seu nome: ")
    tamanho = nome.__len__()
    if tamanho <= 4:
        print("Seu nome é curto")
    elif 5 <= tamanho <= 6:
        print('Seu nome é normal')
    else:
        print('Seu nome é muito grande')


if __name__ == '__main__':
    # exercicio01()
    exercicio02()
    exercicio03()
