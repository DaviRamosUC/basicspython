nome = input('Qual é o seu nome? ')
# Método convêncional
# if nome:
#     print(nome)
# else:
#     print('Você não digitou nada =(')

print(nome or 'Você não digitou nada =(')
print(nome or None or False or 0 or 'Você não digitou nada =(')
