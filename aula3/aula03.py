'''
str - string
'''

print("Essa é uma 'string' (str).")
print('Essa é uma "string" (str).')
print('Essa é uma \'string\' (str).')
print('Essa é uma \n (str).')
print(r'Essa é uma \n (str).')  # r = raw
