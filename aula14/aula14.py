texto = 'python'

# c = 0
# while c < len(texto):
# print(texto[c])
# c += 1

for letra in texto:
    print(letra, end=' ')

print("")

for n, letra in enumerate(texto):
    print(n, letra)

print("#" * 10)

for numero in range(10):
    print(numero)

print("#" * 10)

for numero in range(2, 12, 2):
    print(numero)

print("#" * 10)

for numero in range(20, 10, -1):
    print(numero)

print("#" * 10)
for n in range(100):
    if n % 8 == 0:
        print(n)
