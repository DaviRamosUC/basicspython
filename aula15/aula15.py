# Listas
texto = 'Valor'  # É uma lista de char
print(texto[0], end="\n\n")

#         0    1    2    3    4    5
lista = ['A', 'B', 'C', 'D', 'E', 10.5]  # Não tem tipagem fixa como no Java
#        -6    -5   -4   -3   -2   -1

print(lista[::-1])

lista[5] = 'Qualquer outra coisa'
print(lista, end='\n\n')

l1 = [1, 2, 3]
l2 = [4, 5, 6]
print('Lista inicial l1:', l1)
print('Lista inicial l2:', l2)

l1.extend(l2)  # é a mesma coisa de somar duas listas ex: l1 + l2
l2.insert(0, 'banana')
print('Extendendo o l1 com l2:', l1)
print('adicionando "banana" no indice 0 da l2:', l2)

l2.pop()
print('Removendo último indicie da l2:', l2)
del (l2[0])
print('Removendo primeiro indicie da l2:', l2)

print('#' * 10)

l3 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
del (l3[3:5])
print(l3)

l4 = list(range(1, 10))
print(l4)
